---
title: July/August in KDE PIM
author: Daniel Vrátil
date: 2023-09-01T17:30:00+02:00
url: /blog/2023/2023-09-01-kde-pim-july-august-2023
---

Here's our bi-monthly update from KDE's personal information management applications team, covering progress made in the months of July and August 2023.

Since [the last report](https://volkerkrause.eu/2023/07/05/kde-pim-may-june-2023.html), 38 people contributed approximately
1400 code changes, focusing on bugfixes and improvements for the 23.08 release, as well as preparing for the transition to Qt6.

## Transition to Qt6/KDE Frameworks 6

As we plan for KDE Gear 23.08 to be our last Qt5-based release, there's been on-going work on more Qt6 porting, removing deprecated functions, and so on.

## Itinerary

Itinerary got new importing options, public transport mode preferences, and support for many more travel document formats. It has also been featured prominently on the new [KDE for Travelers](https://kde.org/for/travelers/) page. For more details see [Itinerary's own summary post](https://volkerkrause.eu/2023/08/05/kde-itinerary-june-july-2023.html).

![Transport mode preferences in KDE Itinerary](images/kde-itinerary-transport-mode-preferences.png)

## Merkuro

As part of Gear 23.08, we renamed Kalendar to Merkuro. This has been done since the application not only lets you manage your events and tasks any more, but also incorporates contact management and there is in-progress work to also support email.

For Gear 23.08, we also did a complete revamp of the look of the application and split Merkuro into two applications, so you can have a window with your calendar and another one with your contacts at the same time.

For Gear 23.12, we made the folder list in Merkuro Mail sort the exact same way as KMail by implementing MailCommon::IKernel, added support for libgravatar, and made it possible to edit your IMAP and Identity accounts (including your crypto settings). A big part of the work on supporting Identity accounts went into making KIdentityManagement usable from QML, so other QtQuick-first PIM apps can benefit from it.

As part of GSoC, Anant Verma implemented support for availability ([RFC 7953](https://www.rfc-editor.org/rfc/rfc7953)) in Merkuro as well as adding the infrastructure required for this in KCalendarCore and Akonadi Calendar. The end result is that you can now set which time of the day you are available (e.g. working hours in case of a work calendar) and this will then be displayed as part of your free/busy information for other users.

## KOrganizer

In the past two months, Dan has worked on improving event views in KOrganizer. He reworked the internals of all event views so that, instead of merging all user's calendars into a single huge calendar which then gets displayed, each event view now supports displaying contents of multiple independent calendars. Thanks to this change, if two calendars contain the same event, the event now gets correctly displayed in both calendars. Previously only one instance would randomly get displayed, as the other one was removed from the huge merged calendar as it was considered a duplicate. This is a fairly common situation when a user is subscribed to their colleague's calendar and they both receive invitations to the same meeting, in which case the same event will be present in both their calendars.

While digging through the code and playing with the event views during testing, many smaller issues and bugs have been fixed as well. Most of these will land in KDE Gear 23.12.

![Agenda multi-view showing two calendars with identical meeting invitation](images/korganizer-multiview.png)

* Fixed recurrent events that were planned during DST showing incorrect time when DST ended ([Bug 460186](https://bugs.kde.org/show_bug.cgi?id=460186))
* Copying or moving an event using the mouse in the agenda view no longer loses timezone information ([Bug 448703](https://bugs.kde.org/show_bug.cgi?id=448703))
* Sending a counter-proposal to an invitation uses the correct identity and sender ([Bug 458524](https://bugs.kde.org/show_bug.cgi?id=458524))
* Fixed the Akonadi iCal Directory Resource not storing any events in the directory ([Bug 436693](https://bugs.kde.org/show_bug.cgi?id=436693))

The work on improving KOrganizer and event views was funded by [g10 Code](https://g10code.com).

## KMail

Laurent has started to re-work the existing AdBlock plugin to be based on the ad-blocking code from [AngelFish browser](https://apps.kde.org/angelfish/).

It is now also possible to display the history of received emails. This means that if the user has a lot of folders with many unread messages, it's now easier to find in which folders the user received new emails recently.

The composer now directly shows information about encryption keys that are near their expiry date or which have already expired. Prior version of KMail showed this information after the user hit the Send button via a popup dialog. Now the user can update the key before hitting Send. Trust Levels for encryption keys are shown in tooltips instead of just their validity, as [the concept of Trust Levels](https://wiki.gnupg.org/AutomatedEncryption#Trust_Levels) make more sense in the cotnext of mail than simple key validity. Additionally, the Encrypt button now indicates, whether encryption is possible or not.

![Composer showing trust level of recipients key](images/composer_trust_levels.png)
![Composer showing a warning that recipients key will expire soon](images/composer_near_expiry.png)

* The Archive plugin now supports defining a time range when archiving ([Bug 473007](https://bugs.kde.org/show_bug.cgi?id=473007))
* Fixed "Do not change color from original HTML email" also affecting plain text emails ([Bug 471857](https://bugs.kde.org?show_bug.cgi?id=471857))
* Added support for deleting all attachments from selected messages ([T6577](https://dev.gnupg.org/T6577))

![The Archive plugin with new time-range options](images/archive-folder-range.png)

## KAddressbook

* Added keyboard shortcuts to KAddressbook to make editing content easier ([Bug 473224](https://bugs.kde.org?show_bug.cgi?id=473224))

## Kleopatra

* All places where an expiration date can be set now use a sensible default and respect configurable restrictions ([T6519](https://dev.gnupg.org/T6519)). Moreover, the UI for setting an expiration date has been unified ([T6621](https://dev.gnupg.org/T6621)).
* When updating an OpenPGP key, Kleopatra now queries the certificate directories of providers (WKD) additionally to the configured key server. For privacy reasons, by default, Kleopatra only does this for user IDs which were originally retrieved from a WKD. ([T5903](https://dev.gnupg.org/T5903))
* A new GnuPG Desktop AppImage containing Kleopatra (23.07.70) and a tech preview of a special GnuPG edition of Okular (providing PDF signing with GnuPG) is available at https://gnupg.org.

## KAlarm

* Multiple alarms can be set to wake the system from suspend when they are due. This is only available on Linux systems configured with the capability for users to set kernel alarm timers. For release in KDE Gear 23.12.

## Akonadi

After more than a decade, we finally removed our custom fork of the Qt SQLite driver from Akonadi. The fork was initially created to provide better support for using SQLite from multiple threads, but SQLite itself has matured over the years to a point that the custom code in the fork isn't needed anymore, and so we can just use the upstream Qt SQLite driver. This change is backwards compatible, so users don't need to take any action or modify their configuration. This is yet another step towards making SQLite the default backend for Akonadi.

## Libraries

The code in the akonadi-contacts repository has been split into two libraries: "core" and "widgets". "Core" contains models and formatters reusable in QML-based PIM applications, while the "widgets" library contains QWidget-based UI classes used by KAddressBook and other widget-based PIM apps. Similar work has been done in the LibKSieve repository as well, which now contains KSieveCore and KSieveWidget libraries.

### MimeTreeParser & Kleopatra

We moved the mime tree parser code from Merkuro to a new repository to share as much code as possible with Kleopatra. We are reusing the code in Kleopatra, making it possible to display encrypted emails directly in there. This leads to various improvements in this mime tree parser implementation, in particular around how we handle encryption and support for GPG trust levels.

### GpgME & Craft

There is ongoing work to update the GnuPG stack on craft and make GpgME C++ bindings compile with MSVC (the Microsoft compiler). This would allow us to bring back the PIM stack on Windows.

This work on MimeTreeParser and Craft/GpgME was funded by [g10 Code](https://g10code.com).

### KTextAddons

The custom text editor that we've had in KDE PIM (used e.g. for composing messages) was moved to KTextAddons, allowing other applications like Ruqola to use it as well. The editor supports many features, including translating text via online translators. Laurent has added a new local translator plugin using [Bergamot](https://browser.mt/), a machine learning translation model that runs locally on the user's computer, so that the user does not have to rely on cloud services for translations.

## Snaps

Scarlett has been working on updating the Snaps with latest KDE PIM. If you're a Snaps user, you can look forward to getting the latest and greatest KDE PIM via Snaps.

