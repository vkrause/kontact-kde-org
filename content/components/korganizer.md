---
aliases:
  - /korganizer
layout: component
title: KOrganizer
shortDescription: >
    Organizer your time! Manage events, todos, schedule meetings and more.
description: >
    KOrganizer is a powerful and feature-rich organizer, event and todo manager
    that integrates well with the rest of Kontact, especially with KMail.
screenshot: /assets/img/kontact-korganizer.png
weight: 30
---

KOrganizer is the calendar and scheduling component of Kontact. It provides
management of events and tasks, alarm notification, web export, network 
transparent handling of data, group scheduling, import and export of calendar
files and more. It is able to work together with a wide variety of calendaring
services, including NextCloud, Kolab, Google Calendar and others. KOrganizer
is fully customizable to your needs and is an integral part of the Kontact
suite, which aims to be a complete solution for organizing your personal data.
KOrganizer supports the two dominant standards for storing and exchanging
calendar data, vCalendar and iCalendar.

## Features
* **Support for multiple calendars and todo lists.** Korganizer can transparently
  merge calendar data from different files or other calendar data sources for
  example calendars on the web. They can conveniently be activated, deactivated,
  added and removed from the graphical user interface.
* **Kontact integration.** KOrganizer is fully integrated with Kontact, the
  complete personal information management application. Within Kontact some
  additional features are available like conversion of mails to events or todos
  by drag and drop.
* **Storage model.** KOrganizer has a persistent calendar. The user doesn't have
  to take care of loading or saving the calendar. Changes are immediately saved
  to disk. If the calendar is changed externally it is automatically loaded and
  updated in the view. A locking mechanism handles concurrent access to the
  calendar.
* **Undo and Redo.** KOrganizer supports unlimited undo and redo.
* **Todo integration with agenda view.** Todos are shown in the week and day
  views. Todos can be converted to events by dragging from the todo list and
  dropping on the agenda view.
* **Attachments for events and todos.** References to web pages, local files or
  mails can be attached to events and todos. The attached data can easily be
  accessed by a single click from the event and todo views as well as the
  summary view or the editors
* **Quick todo entry.** A special input field allows to quickly create a todo
  without needing to open an editor. This is especially handy for creating
  multiple todos in a row.
* **Quick event entry.** There are several ways to create events from the agenda
  view: Per type-ahead events can be created by selecting a time range and then
  simply starting to type. An editor will be opened and the type text will be go
  into the title. Optionally the event editor can be opened when the time
  selection is finished and in addition to the usual menu and toolbar entries
  there are key bindings and a context menu to start the editor dialog.
* **Print support.** Calendars can be printed using various different styles.
  Printing also supports colors and overlapping events.
* **KMail Integration.** KMail directly supports transfer of invitations and
  other calendar attachments to KOrganizer.

