# Kontact Website

[![Build Status](https://binary-factory.kde.org/buildStatus/icon?job=Website_kontact-kde-org)](https://binary-factory.kde.org/job/Website_kontact-kde-org/)

This is the git repository for [kontact.kde.org](https://kontact.kde.org), the website for Kontact.

As a (Hu)Go module, it requires both Hugo and Go to work.

### Development
Read about the shared theme at [hugo-kde wiki](https://invent.kde.org/websites/hugo-kde/-/wikis/)

Preview the website locally by starting a local hugo powered web server via

```bash
./server.sh
```

The command will print the URL to use for local previewing.

### I18n
See [hugoi18n](https://invent.kde.org/websites/hugo-i18n)
