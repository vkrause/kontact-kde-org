---
title: March/April in KDE PIM
author: Ingo Klöcker
date: 2023-05-02T18:00:00+00:00
url: /blog/2023/2023-05-03-kde-pim-march-april-2023
---

Here's our bi-monthly update from KDE's personal information management applications team, covering progress made in the months of March and April 2023.

Since [the last report](https://volkerkrause.eu/2023/03/08/kde-pim-january-february-2023.html), 38 people contributed approximately
1700 code changes, focusing on bugfixes and improvements for the 23.04 release, as well as preparing for the transition to Qt 6.

## KDE PIM Sprint

On the first weekend of April, a few of us met in Toulouse for the first post-pandemic KDE PIM Sprint. Kevin has published a [report](https://planet.kde.org/kevin-ottens-2023-04-02-report-from-kdepim-sprint-2023/) with details on everything we did.

## Transition to Qt 6/KDE Frameworks 6

Shortly after the KDE PIM sprint in Toulouse, the switch to Qt 6 and KF6 entered its final phase with the creation of the `kf6` porting branches in the KDE PIM repositories. The aim is to  publish a first release based on that towards the end of the year. See the corresponding [blog post](https://volkerkrause.eu/2023/04/07/kde-pim-kf6-branching.html) for more details.

The `master` branches are used for the remaining Qt 5 based releases.

We fixed some bugs in the kf6 builds, and now kmail/knotes/akregator/etc. can be used. That said, it’s still experimental, and a lot of crashes were reported.

Sandro started updating the kdepim-docker image to support Qt6/KF6 variant. You can now build a Qt6 one with `build.sh -q 6` and run it with `run.sh -q 6`.

## Kalendar

The contact editor was completely redesigned and allows setting a lot more contact information than previously available.

![Screenshot of Kalendar showing the redesigned contact editor](images/kalendar-with-contact-editor.png)

It is also now possible to set custom reminder time.

![Screenshot showing the custom reminder time editor](images/kalendar-custom-reminder.png)

We fixed various small bugs in the UI and a major bug in the `IncidenceOccurenceModel` which caused a crash and affected many users.

We now have more unit tests in many places, thanks to Anant Verma and Joshua Goins.

During the KDE PIM sprint, we managed to significantly reduce the number of libraries we link against which should improve slightly the startup time and reduce the number of dependencies.

## KOrganizer

The search field to filter the collections in the left sidebar now works again, and it's now possible to copy events from read-only calendars ([415774](https://bugs.kde.org/415774)). Besides, KOrganizer won't crash when you drag an event outside of the events view
([461413](https://bugs.kde.org/461413)), and adding a new iCal calendar should now sync properly ([425460](https://bugs.kde.org/425460)).

## Shared calendar infrastructure

Invitations are now displayed correctly when toggling the search collections in the left sidebar of both Kalendar and Korganizer. Akonadi-Search now has some unit tests for the calendar indexer and the code base was modernized.

The initial calendar setup, which configured the default calendar and the search collections for invitations, was moved from Korganizer to Akonadi Calendar which allows both Kalendar and Korganizer to make use of it.

## Akonadi

The SQLite support was improved by enabling the WAL mode and disabling shared cache mode. We also removed old legacy code implemented to support ancient versions of SQLite. The intention was to remove the custom fork of the QSQLITE SQL driver that we had for over a decade, and make SQLite the default database backend in the near future. FreeBSD has already [switched](https://euroquis.nl//freebsd/2023/04/24/akonadi.html).

Moreover, we fixed 3 important high profile bugs in these last two months:

- The MySQL backend runs mysql_upgrade which should ensure the database internal structure is up to date and fix various issues were the logs were quickly filled up with warnings. ([456983](https://bugs.kde.org/456983) and a few similar bug reports)

- A potential crash on shutdown has been fixed ([462692](https://bugs.kde.org/462692)), as well as a curious race condition that was causing instabilities in our test suite.
 
- Jiří Paleček fixed a bug caused by an invalid range iteration which caused various errors ([468343](https://bugs.kde.org/468343) and a few similar bug reports).

## Itinerary

Itinerary received support for numerous additional travel document and ticket formats, as well as additional ways to guide you to the right spot on a railway platform. See it's own [summary post](https://volkerkrause.eu/2023/03/31/kde-itinerary-february-march-2023.html) for more details.

## KAddressBook

Interactions with phone numbers for calls and SMSs now behave like anywhere else on the system, and now actually work for contact data shown inside KMail.

## Text input and understanding

We released version 1.3.0 of KTextAddons. A new library called textemoji was created which allows to select a unicode font emoji. Originally we used a code from kpimtextedit, and it extracted and split as Core/Widgets. This library can be used in the future in QML too, and this widget supports custom emojis too.

![Screenshot showing the emoji picker](images/emoji1.png)

Autocorrection was split between code/widget too, so that it can be used with QML.

### KMail

We rewrote undo send notification. Now we can show "to" info and "title info".

If the de-vs compliance mode is active in GnuPG, then the compliance state is now shown in verification and decryption results and when selecting keys ([T6343](https://dev.gnupg.org/T6343)).

Additionally we fixed these bugs:

- Bug [466931](https://bugs.kde.org/466931), where Custom Message Aggregation Modes were not shown in Message List preferences until a restart.
- Bug [468801](https://bugs.kde.org/468801), in which we removed an unused warning, "unknown mimetype «text/x-moz-deleted»", from org.kde.pim.mimetreeparser.
- Bug [467034](https://bugs.kde.org/467034), in which libksieve/src/kmanagesieve/session.cpp was assigning passwords to usernames and they were geting logged.
- Bug [467291](https://bugs.kde.org/467291), in which switching Folder List layout caused messages in the message list to disappear.

### Kleopatra

g10 Code, the company which funds the development of Kleopatra and improvements in a few other KDE applications, [is becoming a Patron of KDE](https://dot.kde.org/2023/04/25/g10-code-becomes-kde-patron)!

In technical news, when moving a decrypted archive to its final destination, you can now see the progress ([T6373](https://dev.gnupg.org/T6373)), and checking certificates for compliance was unified ([T6380](https://dev.gnupg.org/T6380)).

The notepad now remembers the certificates that were last used when signing/encrypting the entered text ([T6415](https://dev.gnupg.org/T6415)). Also, when signing or encrypting files, Kleopatra now shows hints if the selected certificates are about to expire soon ([T6330](https://dev.gnupg.org/T6330)), and The default expiration time of certifications can now be configured ([T6452](https://dev.gnupg.org/T6452)).

![Screenshot showing Kleopatra's Sign/Encrypt dialog with hints about soon expiring certificates](images/kleopatra-expiring-certificates.png)

Kleopatra no longer offers encryption-only OpenPGP certificates as signing certificates ([T6456](https://dev.gnupg.org/T6456)), and some problems with Unicode characters in error messages where fixed on Windows ([T5960](https://dev.gnupg.org/T5960)).
