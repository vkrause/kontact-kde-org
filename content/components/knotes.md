---
aliases:
  - /knotes
layout: component
title: KNotes
shortDescription:
description:
weight: 50
---

KNotes is a program that lets you write the computer equivalent of sticky notes.
The notes are saved automatically when you exit the program, and they display
when you open the program.

## Features

* Write notes in your choice of font and background color
* Use drag and drop to email your notes
* Can be dragged into Calendar to book a time-slot
* Notes can be printed
