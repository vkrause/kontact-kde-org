---
aliases:
  - /kmail
layout: component
title: KMail
screenshot: /assets/img/kontact-kmail.png
shortDescription: Boost your productivity with a feature-rich email application.
description: >
    KMail is a state-of-the-art email client that integrates well with widely
    used email providers like GMail. It provides many tools and features to
    maximize your productivity and makes working with large email accounts
    easy and fast. KMail supports a large variety of email protocols - POP3,
    IMAP, Microsoft Exchange (EWS) and more.
weight: 20
---

KMail is the email component of Kontact, the integrated personal information
manager from KDE.

## Secure

* **Secure Default.** Default settings rather compromise on features than
  security or privacy.
* **End-to-end Encryption.** Support for OpenPGP and S/MIME is built-in,
  and a setup and key generation wizard aids users in getting started with
  this.
* **Transport Encryption.** SSL/TLS encrypted communication is of course
  supported, as are a number of authentication methods such as GSSAPI
  (Kerberos) or OAuth2.
* **Strong isolation of HTML content.** If HTML emails can't be avoided,
  KMail ensures no external references can be hidden in there to leak
  information or compromise your privacy. Additionally KMail's phishing
  protections warns about suspicious links in such emails.
* **Spam Protection.** If your email server doesn't take care of this already,
  KMail can integrate popular spam checkers such as SpamAssassin or Bogofilter
  locally.

## Powerful
* **Offline Capable.** KMail allows you to operate entirely without network
  connectivity by (optionally) syncing your entire email locally.
* **Multiple Identities.** KMail separates identities from accounts, giving
  you a lot of flexibility when wearing different hats in multiple organizations.
* **Templates.** A flexible template and text snippet system support you in
  automating parts of your email writing.
* **Multi-lingual.** Built-in content translation support and per-paragraph
  spell checking language auto-detection support you when dealing with content
  in multiple languages.
* **Productive Composer.** KMail's email composer comes with many small helpers
  to make you more efficient, such as warning about forgotten attachments,
  inline attachment compression or resizing of attached images.
* **Powerful Filtering.** Besides automating the sorting of your emails by a
  multitude conditions, KMail's filtering system can also be used for
  implementing arbitrary workflows as it can integrate with external
  applications.
* **Searching and Tagging.** Flagging and tagging of messages aid in sorting
  and information recovery, and so do the powerful local and remote searching
  capabilities.
* **Mailing List Management.** Mailman mailing lists can be auto-detected and
  common mailing list management operations are directly available from within
  KMail.
* **Flexible Configuration.** Countless configurations options allow you to tweak
  many aspects of KMail to exactly your needs and wishes.

## Integrated
* **iTip Invitations.** Meeting invitation emails are detected and offer in-place
  quick responses, and are of course fed into [KOrganizer](../korganizer).
* **Addressbook.** Besides auto-completing address from [KAddressBook](../kaddressbook),
  KMail also uses avatars and crypto preferences from there.
* **Booking Emails.** KMail can detect flight or hotel reservations in emails
  from certain providers and offers to add them to your calendar.

## Standard Compliant
* Supports the standard mail protocols IMAP, POP3 and SMTP.
* Supports push email (IMAP IDLE).
* Searching in IMAP folders fully supported.
* Native support for inline OpenPGP, PGP/MIME, and S/MIME.
* Support for Sieve server-side filtering.

